#!/usr/bin/env bash
#
while :; do
        echo " __________________________________ "
        echo "|          Managing Files          |"
        echo "|                                  |"
        echo "|     (R)ename or (M)ove Files     |"
        echo "|__________________________________|"
        echo "Select an option: "
        read option
        clear
        case "$option" in
                r|R)
                        echo " --------- Rename File ---------- "
                        echo "What folder do you want to rename?"
                        echo "----------------------------------"
                        echo "$(ls "$TORRENT/")"
                        echo "----------------------------------"
                        read folderBefore
                        echo "----------------------------------"
                        echo "What name do you want?"
                        read folderAfter
                        mv "$TORRENT/$folderBefore" "$TORRENT/$folderAfter"
                        clear
                        echo " --------- Move File ------------ "
                        echo "What is the destination folder?"
                        echo "----------------------------------"
                        echo "$(ls "$GDRIVE/")"
                        echo "----------------------------------"
                        read destination
                        clear
                        echo "##################################"
                        #
                        # Create and sync the folder
                        #
                        mkdir "$GDRIVE/$destination/$folderAfter"
                        # Check if the folder exists at destination
                        echo "The folder $folderAfter were created"
                        echo "----------------------------------"
                        rclone copy --no-traverse --progress "$TORRENT/$folderAfter" "GDrive:/$destination/$folderAfter"
                        #
                        # Now, checks if the movement was done
                        #
                        if [ -d "$GDRIVE/$destination/$folderAfter/" ]
                        then
                        # Check if the folder is empty or not
                                find "$GDRIVE/$destination/$folderAfter" -type f -exec echo Found file {} \;
                                echo "----------------------------------"
                                echo -n "What next? (D)elete folder/(K)eep folder/(E)xit? "
                                read option
                        # Set options to delete or keep the source folder and files
                                case "$option" in
                                        d|D)
                                                echo "Deleting source folder $folderAfter."
                                                rm -R "$TORRENT/$folderAfter"
                                                echo "Deleted"
                                                ;;
                                        k|K)
                                                echo "Keeping folder $folderAfter as it is."
                                                echo "Done"
                                                ;;
                                        e|E)
                                                echo "Exiting without changes..."
                                                echo "Done"
                                                ;;
                                        *)
                                                echo "Invalid option"
                                                ;;
                                esac
                        else
                                echo "Nope, it wasn't moved"
                        fi
                        clear
                        ;;
                m|M)
                        echo " --------- Move File ------------ "
                        echo "What is the destination folder?"
                        echo "----------------------------------"
                        echo "$(ls "$GDRIVE/")"
                        echo "----------------------------------"
                        read destination
                        #
                        echo "What is the folder to be uploaded?"
                        echo "----------------------------------"
                        echo "$(ls "$TORRENT/")"
                        echo "----------------------------------"
                        read folder
                        clear
                        echo "##################################"
                        #
                        # Create and sync the folder
                        #
                        mkdir "$GDRIVE/$destination/$folder"
                        # Check if the folder exists at destination
                        echo "The folder $folder were created"
                        echo "----------------------------------"
                        rclone copy --no-traverse --progress "$TORRENT/$folder" "GDrive:/$destination/$folder"
                        #
                        # Now, checks if the movement was done
                        #
                        if [ -d "$GDRIVE/$destination/$folder/" ]
                        then
                                echo "----------------------------------"
                        # Check if the folder is empty or not
                                find "$GDRIVE/$destination/$folder" -type f -exec echo Found file {} \;
                                echo "----------------------------------"
                                echo -n "What next? (D)elete folder/(K)eep folder/(E)xit? "
                                read option
                        # Set options to delete or keep the source folder and files
                                case "$option" in
                                        d|D)
                                                echo "Deleting source folder $folder."
                                                rm -R "$TORRENT/$folder"
                                                echo "Deleted"
                                                ;;
                                        k|K)
                                                echo "Keeping folder $folder as it is."
                                                echo "Done"
                                                ;;
                                        e|E)
                                                echo "Exiting without changes..."
                                                echo "Done"
                                                ;;
                                        *)
                                                echo "Invalid option"
                                                ;;
                                esac
                                else
                                echo "Nope, it wasn't moved"
                        fi
                        clear
                        ;;
                *)
                        echo "Invalid option"
                        ;;
        esac
        clear
        echo "-------------------------------"
        read -n 1 -s -r -p "To finish, hit [CTRL+C]"
        clear
done
