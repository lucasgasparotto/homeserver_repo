pipeline {
    agent any 
    parameters {
        string(name: 'PKG', defaultValue: 'package.deb', description: 'Link to download the package')
        string(name: 'FILENAME', defaultValue: 'install.deb', description: 'Set the filename to install package')
        string(name: 'SERVICE', defaultValue: 'service', description: 'Service to be restarted')
    }
    stages {
        stage ('Download Package') {
            steps {
                sh "sudo wget -c ${params.PKG} --output-document ./${params.FILENAME}"
            }
        }
        stage ('Install package') {
            steps {
                sh "sudo dpkg -i ./${params.FILENAME}  && apt install -f"
            }
        }
        stage ('Restart service') {
            steps {
                sh "sudo systemctl start $params.SERVICE}"
            }
        }
        stage ('Clean installation folder') {
            steps {
                sh "sudo rm ./${params.FILENAME}"
            }
        }
    }
}