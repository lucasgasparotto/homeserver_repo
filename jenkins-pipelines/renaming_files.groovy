pipeline {
    agent any 
    parameters {
        string(name: 'ROOT', defaultValue: '"set_root_folder"', description: 'Folder location')
        string(name: 'FOLDER', defaultValue: '"fill"', description: 'File or folder to be renamed')
        string(name: 'NEWNAME', defaultValue: '"new_name"', description: 'Set a new name')
    }
    stages {
        stage ('Rename File or Folder') {
            steps {
                sh "sudo mv /home/lgasparotto/${params.ROOT}/${params.FOLDER}/ /home/lgasparotto/${params.ROOT}/${params.NEWNAME}/"
            }
        }
        stage ('Echo new folders Content') {
            steps {
                sh "sudo ls -a /home/lgasparotto/${params.ROOT}/${params.NEWNAME}"
            }
        }
    }
}