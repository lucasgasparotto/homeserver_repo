pipeline {
    agent any 
    parameters {
        string(name:'FOLDER', defaultValue: '"test"', description: 'Folder to be sent to GDrive')
        string(name:'DESTINATION', defaultValue: '"Destination"', description: 'Destination folder')
    }
    stages {
        stage ('Create destination folder') {
            steps {
                sh "mkdir /home/lgasparotto/GDrive/${params.DESTINATION}/${params.FOLDER}"
            }
        }
        stage ('Begin copy') {
            steps {
                sh "rclone copy --no-traverse --progress /home/lgasparotto/Downloads/${params.FOLDER} GDrive:${params.DESTINATION}/${params.FOLDER}"
            }
        }
        stage ('Checking') {
            steps {
                sh "if [ -d /home/lgasparotto/GDrive/${params.DESTINATION}/${params.FOLDER} ]; then sudo rm -Rf /home/lgasparotto/Downloads/${params.FOLDER}/; else echo 'Nothing Happened'; fi"
            }
        }
    }
}