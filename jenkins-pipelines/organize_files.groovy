pipeline {
    agent any 
    parameters {
        string(name: 'ROOTFOLDER', defaultValue: '"rootfolder"', description: 'Root folder')
        string(name: 'FOLDER', defaultValue: '"folder"', description: 'Main folder')
        string(name: 'DELFOLDER', defaultValue: '"delfolder"', description: 'Folder to delete')
        string(name: 'PERMISSIONS', defaultValue: '0000', description: 'Set folder permissions')
    }
    stages {
        stage ('Open Folder') {
            steps {
                sh "cd /home/lgasparotto/${params.ROOTFOLDER}/${params.FOLDER}/"
            }
        }
        stage ('Open folder to be deleted') {
            steps {
                sh "cd ${params.DELFOLDER}/"
            }
        }
        stage ('Move files') {
            steps {
                sh "sudo mv * /home/lgasparotto/${params.ROOTFOLDER}/${params.FOLDER}/"
                sh "cd .."
            }
        }
        stage ('Delete folder') {
            steps {
                sh "sudo rmdir ${params.DELFOLDER}/"
            }
        }
    }
}