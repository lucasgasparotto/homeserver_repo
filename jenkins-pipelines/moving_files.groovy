pipeline {
    agent any 
    parameters {
        string(name: 'ROOT', defaultValue: '"set_root_folder"', description: 'Folder location')
        string(name: 'FILE', defaultValue: '"fill"', description: 'File to be moved')
        string(name: 'FOLDER', defaultValue: '"new_folder"', description: 'Brand new folder')
    }
    stages {
        stage ('Create Folder') {
            steps {
                sh "sudo mkdir /home/lgasparotto/${params.ROOT}/${params.FOLDER}/"
            }
        }
        stage ('Move File') {
            steps {
                sh "sudo mv /home/lgasparotto/${params.ROOT}/${params.FILE} /home/lgasparotto/${params.ROOT}/${params.FOLDER}/"
            }
        }
        stage ('Echo the content of the new folder') {
            steps {
                sh "sudo ls -l /home/lgasparotto/${params.ROOT}/${params.FOLDER}/"
            }
        }
    }
}