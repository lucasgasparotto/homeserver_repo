@ECHO OFF

SET FIRST_PARAM=%1
IF %FIRST_PARAM% == "" goto :USAGE

SETLOCAL
SET PATH=%PATH%;C:\Program Files\Puppet Labs\Puppet\sys\ruby\bin
echo --------------------------------------------------------------------------
echo ### Editing ###
echo.
call eyaml edit --pkcs7-public-key=C:\sovosRepositories\beta_repo\Testing\public_key.pkcs7.pem --pkcs7-private-key=C:\sovosRepositories\beta_repo\Testing\private_key.pkcs7.pem %1
echo.

ENDLOCAL
GOTO EOF

:USAGE
echo Use eyaml_edit.cmd "[path to eyaml from root]"
goto EOF

:FILENOTFOUND
echo ### Please run the batch file from the "Tools" or the "root" folder of the repo
goto EOF

:EOF
